FROM tigerseo/rpi2-archlinuxarm

RUN pacman -S mongodb --noconfirm
RUN mkdir -p /data/db

VOLUME /data/db

EXPOSE 27017

WORKDIR /data/db

CMD ["mongod", "--journal", "--storageEngine=mmapv1"]
